import {type Product } from "../types";

export default function Products  (
    { products }:
        { products: Product[] }

) {
    return (
        <ul className="grid grid-cols-[repeat(auto-fill,minmax(250px,1fr))] gap-4">
            {products.map(({ id, thumbnail, title, currency_id: currencyId, price }) => (
                <li key={id} className="border rounded p-4">
                    <img src={thumbnail} alt={title} className="w-full h-48 object-cover mb-4" />
                    <h3 className="text-lg font-semibold mb-2">{title}</h3>
                    <strong className="text-gray-700">{price.toLocaleString("es-AR", {
                        currency: currencyId,
                        style: "currency"
                    })}</strong>
                </li>
            ))}
        </ul>
    );
}