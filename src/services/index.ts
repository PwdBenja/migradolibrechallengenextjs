import { Category, type Product } from '../types'
import { SELLER_ID, API_URL } from '@/config'

export const getProducts = (category?: string) => {

    const url = new URL(`${API_URL}/sites/MLA/search`)
    
    if (!SELLER_ID) throw new Error('no seller id provided')

    url.searchParams.append('seller_id', SELLER_ID)

    if (category) url.searchParams.append("category", category)

    return (
        fetch(`${url}`)
            .then((res) => res.json() as Promise<{ results : Product[]}>)
            .then((res) => {
                    console.log(res.results)
                    return res.results
            })
    )
}

export const getCategory = async (products : Product[]) => {
    
    const ids = Array.from (
        new Set(products.map(products => products.category_id))
    )

    const allProductCategories = await Promise.all (
        ids.map((id) => 
            fetch(`https://api.mercadolibre.com/categories/${id}`)
                .then((res) => res.json() as Promise <{path_from_root: {id: string; name: string}[]}>)
                .then((res) => res.path_from_root)
        )
    )

    const draft : Record<string,Category> = {}

    allProductCategories.forEach((productCategories) => {
        productCategories.forEach((singleCategory, index) => {
            const { id } = singleCategory;
            const parent = productCategories[index - 1] as Category | undefined;
            const parentId = parent?.id;

            draft[id] = { ...singleCategory, parentId }
        })
    })

    return Object.values(draft)
}